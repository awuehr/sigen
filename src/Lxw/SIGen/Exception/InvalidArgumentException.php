<?php

/**
 * @package    SIGen
 * @subpackage Exception
 * @author     Alexander Wühr <awuehr@gmail.com>
 * @copyright  2013 Alexander Wühr <awuehr@gmail.com>
 * @license    http://opensource.org/licenses/MIT  The MIT License (MIT)
 * @link       https://bitbucket.org/awuehr/sigen/
 */

namespace Lxw\SIGen\Exception;

/**
 * Exception which is thrown whenever invalid arguments are passed to a method of any SIGen class
 */
class InvalidArgumentException extends Exception {

}
