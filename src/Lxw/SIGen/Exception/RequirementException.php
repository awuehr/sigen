<?php

/**
 * @package    SIGen
 * @subpackage Exception
 * @author     Alexander Wühr <awuehr@gmail.com>
 * @copyright  2013 Alexander Wühr <awuehr@gmail.com>
 * @license    http://opensource.org/licenses/MIT  The MIT License (MIT)
 * @link       https://bitbucket.org/awuehr/sigen/
 */

namespace Lxw\SIGen\Exception;

/**
 * Exception which will be thrown when a method's requirements are not met
 */
class RequirementException extends Exception {

}
