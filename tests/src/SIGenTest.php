<?php

/**
 * @package    SIGen
 * @subpackage Exception
 * @author     Alexander Wühr <awuehr@gmail.com>
 * @copyright  2013 Alexander Wühr <awuehr@gmail.com>
 * @license    http://opensource.org/licenses/MIT  The MIT License (MIT)
 * @link       https://bitbucket.org/awuehr/sigen/
 */

namespace Lxw\SIGen;

require_once __DIR__.'/../../src/Lxw/SIGen.php';

class SIGenTest extends \PHPUnit_Framework_TestCase {

	public function classNameProvider() {
		return array(
			array('\Lxw\SIGen\InterfaceBuilder'), array('\Lxw\SIGen\Proxy\SimpleProxy'),
			array('\Lxw\SIGen\Exception\Exception'), array('\Lxw\SIGen\Exception\EvalException'),
			array('\Lxw\SIGen\Exception\InvalidArgumentException'),
			array('\Lxw\SIGen\Exception\RequirementException'),
		);
	}

	/**
	 * @test
	 * @testdox All SIGen classes are available
	 * @dataProvider classNameProvider
	 */
	public function testClassLoader($classname) {
		$this->assertTrue(class_exists($classname, true));
	}
}
